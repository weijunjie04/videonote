![index](https://gitee.com/weijunjie04/bolgimage/raw/master/img/24-05-10--21:34:56--4e434b--0b6db54f989de9ab772a42afcc7ac572--index.png)

# 已完成功能

## 添加记录：

![add_sub](https://gitee.com/weijunjie04/bolgimage/raw/master/img/24-05-10--21:35:39--8e126d--5cafc171aaedd63ab83d6c02ea55db0d--add_sub.png)

​	点击添加记录时能够隐藏其他功能键，显示子菜单栏。

​	点击添加记录时，能自动导入上次的记录(并显示上次添加的日期时间)，以便修改。

​	支持的选择类别有：<u>电视剧</u>、<u>电影</u>、<u>动画</u>、<u>动漫</u>、<u>纪录片</u>。

​	**共有三个子菜单键：**

​			<u>“取消”、“确定”、“清空”</u>

​			**取消：**

​					能够将子菜单栏隐藏，并显示出其他功能键。

​			**确定：**

​					能够获取<u>视频名称</u>、<u>视频类别</u>、<u>当前集数</u>并进行写入数据库中，并将子菜单栏隐藏，并显示出其他功能键。

![add_sure](https://gitee.com/weijunjie04/bolgimage/raw/master/img/24-05-10--21:36:05--2bf385--fd34385f4048fa8a6feb8dc4d61e3a95--add_sure.png)

​			**清空：**

​					能够将默认导入是上次记录清空，以便用户进新的记录添加。

![add_clear](https://gitee.com/weijunjie04/bolgimage/raw/master/img/24-05-10--21:36:02--d99eaf--8d7c4d7a80ad69c2eb893456fc1343eb--add_clear.png)

## 查询记录

默认查询近5条记录，并将记录打印到页底的ListView中。

用户可进行增加或减少记录。

![query](https://gitee.com/weijunjie04/bolgimage/raw/master/img/24-05-10--21:36:13--175f02--0e114619035b42b54b5a43c656dfe336--query.png)

## 设置

目前具有<u>删除所有记录</u>：

​		删除数据库所有数据，需要输入密码(默认密码为root)

![settings](https://gitee.com/weijunjie04/bolgimage/raw/master/img/24-05-10--21:36:18--0dad79--cc5c8fdefcede26a455683cca48adcef--settings.png)

![image-20240510213853440](https://gitee.com/weijunjie04/bolgimage/raw/master/img/24-05-10--21:38:57--7309ca--08a53d163727b73bb77389d85f4c4339--image-20240510213853440.png)

![log](https://gitee.com/weijunjie04/bolgimage/raw/master/img/24-05-10--21:38:17--caccbc--253ad8e21e0cd87e880bb3106bce1e49--log-171534829640913.png)

# 未完成功能

用户自定义查询状态为升序 || 降序，

用户自定义通过ID、视频名称、创建日期查询数据。

添加记录时候可以添加图片，以展示视频封面。

查询记录时长按可展开视频简介。

查询记录使用下拉刷新并添加多n条记录。

查询记录时长按ListView中的选项将其选择的选项内容添加至添加记录的EditText中。

用户自定义通过ID、视频名称、创建日期删除数据。

优化ListView(在下滑时出现数据错乱)。