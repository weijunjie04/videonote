package com.android.videonote.DBHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.android.videonote.VideoWatchNote.VideoWatchNote;

import java.util.ArrayList;
import java.util.List;

public class DBHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "video.db";        //数据库名
    private static final String TABLE_NAME = "videoWatchNote";
    private static final int DB_VERSION = 1;        //数据库版本
    private static DBHelper helper = null;      //帮助器实例
    private SQLiteDatabase readDB = null;     //读连接
    private SQLiteDatabase writeDB = null;        //写连接


    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);      //子类构造方法中调用父类构造方法，传入[上下文， 数据库名， 游标工厂， 数据库版本]
    }


    //利用单例模式获取数据库帮助器的唯一实例
    public static DBHelper getInstance(Context context) {
        //如果helper为空，则new一个实例，否则直接返回
        if (helper == null) {
            helper = new DBHelper(context);
        }
        return helper;
    }


    //打开数据库读连接
    public SQLiteDatabase openReadLink() {
        if (readDB == null || readDB.isOpen()) {
            readDB = helper.getReadableDatabase();
        }
        return readDB;
    }


    //打开数据库写连接
    public SQLiteDatabase openWriteLink() {
        if (writeDB == null || writeDB.isOpen()) {
            writeDB = helper.getWritableDatabase();
        }
        return writeDB;
    }


    //关闭数据库连接
    public void closeLink() {
        //关闭读连接
        if (readDB != null && readDB.isOpen()) {
            readDB.close();
            readDB = null;
        }


        //关闭写连接
        if (writeDB != null && readDB.isOpen()) {
            writeDB.close();
            writeDB = null;
        }
    }


    //用于创建数据库
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //定义sql语句
        String sql = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                "videoName TEXT NOT NULL," +
                "videoCoCategory CHAR(5) NOT NULL," +
                "videoNumber INTEGER NOT NULL," +
                "dateTime TEXT NOT NULL" +
                " );";

        //执行sql语句
        sqLiteDatabase.execSQL(sql);
    }


    //当数据库的版本进行更新时会运行，可以进行添加或删除字段……
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }


    //向数据库中添加数据
    public long insert(String videoName, String videoCoCategory, int videoNumber, String dateTime) {
        ContentValues values = new ContentValues();

        values.put("videoName", videoName);
        values.put("videoCoCategory", videoCoCategory);
        values.put("videoNumber", videoNumber);
        values.put("dateTime", dateTime);

        /*
            执行插入记录动作，语句返回插入记录的行号
            如果第三个参数values为null或元素个数为0，由于insert()方法要求必须添加一条主键之外其他字段为NULL的记录，
            为了满足SQL语法的需要，insert语句必须给定一个字段名，如：insert into person(name) values(NULL)，
            倘若不给定字段名，insert语句就成了这样：insert into person() values()，显然这不满足SQL的语法，
            如果第三个参数values不为null并且元素的个数大于0，可以把第二个参数设置为null。
         */
        return writeDB.insert(TABLE_NAME, null, values);
    }

    public long insert(VideoWatchNote videoWatchNote) {
        ContentValues values = new ContentValues();

        values.put("videoName", videoWatchNote.videoName);
        values.put("videoCoCategory", videoWatchNote.videoCoCategory);
        values.put("videoNumber", videoWatchNote.videoNumber);
        values.put("dateTime", videoWatchNote.dateTime);

        /*
            执行插入记录动作，语句返回插入记录的行号
            如果第三个参数values为null或元素个数为0，由于insert()方法要求必须添加一条主键之外其他字段为NULL的记录，
            为了满足SQL语法的需要，insert语句必须给定一个字段名，如：insert into person(name) values(NULL)，
            倘若不给定字段名，insert语句就成了这样：insert into person() values()，显然这不满足SQL的语法，
            如果第三个参数values不为null并且元素的个数大于0，可以把第二个参数设置为null。
         */
        return writeDB.insert(TABLE_NAME, null, values);
    }


    //    向数据库中删除数据
//    public long deleteById(String id) {
//        return writeDB.delete(TABLE_NAME, "id = ?", new String[]{id});
//    }
    public void deleteAllData() {
        SQLiteDatabase database = getWritableDatabase();
        database.delete(TABLE_NAME, null, null);
    }


    //向数据库中修改数据
//    public long updata(User user){
//        ContentValues values = new ContentValues();
//        return writeDB.update(TABLE_NAME, values, "name = ?", new String[]{user.id});       //根据ID号修改
//    }


    //    向数据库中查询所有数据
    public List<VideoWatchNote> queryWatchNoteNumberOfItems(int numberOfItems) {
        List<VideoWatchNote> list = new ArrayList<>();

        //执行记录查询动作，返回结果集的游标
        Cursor cursor = readDB.query(TABLE_NAME, null, null, null, null, null, "_id DESC LIMIT " + numberOfItems);

        //循环去除游标指向的每条记录
        while (cursor.moveToNext()) {
            VideoWatchNote videoWatchNote = new VideoWatchNote();
            videoWatchNote._id = cursor.getInt(0);
            videoWatchNote.videoName = cursor.getString(1);
            videoWatchNote.videoCoCategory = cursor.getString(2);
            videoWatchNote.videoNumber = cursor.getInt(3);
            videoWatchNote.dateTime = cursor.getString(4);
            list.add(videoWatchNote);
        }
        return list;
    }

    public String[] queryLastRecord() {
        String[] result = new String[4];

        //执行记录查询动作，返回结果集的游标
        Cursor cursor = readDB.query(TABLE_NAME, null, null, null, null, null, "_id DESC LIMIT 1");

        while (cursor.moveToNext()) {
            result[0] = cursor.getString(1);
            result[1] = cursor.getString(2);
            result[2] = cursor.getString(3);
            result[3] = cursor.getString(4);
        }

        return result;
    }


    public int queryLastRecordById() {
        int[] result = new int[1];

        //执行记录查询动作，返回结果集的游标
        Cursor cursor = readDB.query(TABLE_NAME, null, null, null, null, null, "_id DESC LIMIT 1");

        while (cursor.moveToNext()) {
            result[0] = cursor.getInt(0);
        }

        return result[0];
    }


//    //向数据库中查询指定数据
//    public List<User> queryById(String username){
//        List<User> list = new ArrayList<>();
//
//        //执行记录查询动作，返回结果集的游标
//        Cursor cursor = readDB.query(TABLE_NAME, null, "id = ?", new String[]{username}, null, null, null, null);
//
//        //循环去除游标指向的每条记录
//        while (cursor.moveToNext()){
//            User user = new User();
//            user.id = cursor.getString(0);
//            list.add(user);
//        }
//        return list;
//    }
}
