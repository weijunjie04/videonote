package com.android.videonote;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.videonote.DBHelper.DBHelper;
import com.android.videonote.Util.GetSystemDateTime;
import com.android.videonote.VideoWatchNote.VideoWatchNote;
import com.android.videonote.VideoWatchNote.VideoWatchNoteAdapter;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    GetSystemDateTime getSystemDateTime = new GetSystemDateTime();      //调用类，获取当前系统的日期和时间

    Handler handler = new Handler();

    DBHelper helper;       //获取helper对象

    ImageButton ib_settings;

    TextView tv_settings_delAllData, tv_settings_delAllData_sure, tv_settings_delAllData_cancel, tv_settings_delDataCustom,
            tv_addNote, tv_addNote_lastRecord, tv_addNote_sure, tv_addNote_cancel, tv_addNote_clear,
            tv_queryNote, tv_queryNote_subT, tv_queryNote_addT;

    RelativeLayout rl_navigation;

    LinearLayout ll_settings, ll_settings_delAllData, ll_function, ll_showData, ll_addNote, ll_addNote_lastRecord;

    Spinner sp_videoCategory;

    EditText et_password, et_videoName, et_videoNumber;

    String[] items;

    String videoName = "";
    String videoCoCategory = "电视剧";
    int videoNumber = 0;       //视频集数

    String passWord = "root";       //用户密码

    int numberOfItems = 0;      //查询记录的个数


    ArrayAdapter<String> adapter;       //将字符串数组与ListView或Spinner等视图组件关联的适配器。


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_settings_delAllData = findViewById(R.id.tv_settings_delAllData);
        tv_settings_delAllData_sure = findViewById(R.id.tv_settings_delAllData_sure);
        tv_settings_delAllData_cancel = findViewById(R.id.tv_settings_delAllData_cancel);
        tv_settings_delDataCustom = findViewById(R.id.tv_settings_delDataCustom);
        tv_addNote = findViewById(R.id.tv_addNote);
        tv_addNote_lastRecord = findViewById(R.id.tv_addNote_lastRecord);
        tv_addNote_sure = findViewById(R.id.tv_addNote_sure);
        tv_addNote_cancel = findViewById(R.id.tv_addNote_cancel);
        tv_addNote_clear = findViewById(R.id.tv_addNote_clear);
        tv_queryNote = findViewById(R.id.tv_queryNote);
        tv_queryNote_subT = findViewById(R.id.tv_queryNote_subT);
        tv_queryNote_addT = findViewById(R.id.tv_queryNote_addT);

        rl_navigation = findViewById(R.id.rl_navigation);

        ll_settings = findViewById(R.id.ll_settings);
        ll_settings_delAllData = findViewById(R.id.ll_settings_delAllData);
        ll_function = findViewById(R.id.ll_function);
        ll_showData = findViewById(R.id.ll_showData);
        ll_addNote = findViewById(R.id.ll_addNote);
        ll_addNote_lastRecord = findViewById(R.id.ll_addNote_lastRecord);

        sp_videoCategory = findViewById(R.id.sp_videoCategory);

        et_password = findViewById(R.id.et_password);
        et_videoName = findViewById(R.id.et_videoName);
        et_videoNumber = findViewById(R.id.et_videoNumber);

        ib_settings = findViewById(R.id.ib_settings);


        tv_settings_delAllData.setOnClickListener(this);
        tv_settings_delAllData_sure.setOnClickListener(this);
        tv_settings_delAllData_cancel.setOnClickListener(this);
        tv_settings_delDataCustom.setOnClickListener(this);
        tv_addNote.setOnClickListener(this);
        tv_addNote_sure.setOnClickListener(this);
        tv_addNote_cancel.setOnClickListener(this);
        tv_addNote_clear.setOnClickListener(this);
        tv_queryNote.setOnClickListener(this);
        tv_queryNote_subT.setOnClickListener(this);
        tv_queryNote_addT.setOnClickListener(this);


        ib_settings.setOnClickListener(this);


        initSpinnerData();
    }


    @Override
    protected void onStart() {
        super.onStart();
        helper = DBHelper.getInstance(this);       //获取helper对象
        helper.openWriteLink();     //打开写连接
        helper.openReadLink();      //打开读连接
    }


    @Override
    protected void onStop() {
        super.onStop();
        helper.closeLink();

    }


    private void initSpinnerData() {
        items = new String[]{"电视剧", "电影", "动画", "动漫", "记录片"};
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_videoCategory.setSelection(0);
        sp_videoCategory.setAdapter(adapter);
        sp_videoCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = (String) parent.getItemAtPosition(position);
                videoCoCategory = selectedItem;
//                Toast.makeText(MainActivity.this, "选中项: " + selectedItem, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // 当没有选择项时触发
            }
        });
    }


    private void addDataToDB() {
        videoName = et_videoName.getText().toString().trim();

        initSpinnerData();

        //如果数据不为空
        if (!videoName.equals("")) {
            //集数为空，则默认设为0
            if (et_videoNumber.getText().toString().trim().equals("")) {
                videoNumber = 0;
            } else {
                videoNumber = Integer.parseInt(et_videoNumber.getText().toString().trim());
            }


            String dateTime = getSystemDateTime.getSystemDateTime();

            VideoWatchNote videoWatchNote = new VideoWatchNote(videoName, videoCoCategory, videoNumber, dateTime);

            //如果大于0，则表示添加成功
            if (helper.insert(videoWatchNote) > 0) {
                Toast.makeText(this, "添加成功\n名称: " + videoName + "\n类别: " + videoCoCategory + "\n集数: " + videoNumber + "\n" + dateTime, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "添加失败", Toast.LENGTH_SHORT).show();
            }

            et_videoName.setText("");
            et_videoNumber.setText("");

            tv_queryNote.setVisibility(View.VISIBLE);
            ll_addNote.setVisibility(View.GONE);
        } else {
            tv_addNote.setText("数据不能为空!!");
            tv_addNote.setTextColor(Color.rgb(255, 0, 0));

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    tv_addNote.setText("添加记录");
                    tv_addNote.setTextColor(Color.rgb(0, 0, 0));
                }
            }, 3000);

        }
    }


    //显示数据
    public void showListView(int numberOfItems) {
        // 获取ListView实例
        ListView listView = findViewById(R.id.lv_ListView);

        listView.setOnScrollListener(null);

        listView.setAdapter(null);      // 清除当前适配器

        // 查询数据并转换为列表
        List<VideoWatchNote> list = helper.queryWatchNoteNumberOfItems(numberOfItems);

        // 创建并设置适配器
        VideoWatchNoteAdapter adapter = new VideoWatchNoteAdapter(this, list);
        listView.setAdapter(adapter);
    }



    @Override
    public void onClick(View v) {
        int lastRecordById = helper.queryLastRecordById();      //查询当前数据库中最大的ID号
        if (v.getId() == R.id.ib_settings) {     //设置按钮
            if (ll_settings.getVisibility() == View.GONE) {
                ll_settings.setVisibility(View.VISIBLE);

                tv_settings_delAllData.setVisibility(View.VISIBLE);
                tv_settings_delAllData.setText("删除所有记录");
                tv_settings_delAllData.setTextColor(Color.rgb(0, 0, 0));
                ll_settings_delAllData.setVisibility(View.GONE);
                tv_settings_delDataCustom.setVisibility(View.VISIBLE);

                ll_addNote.setVisibility(View.GONE);
                ll_showData.setVisibility(View.GONE);

                tv_addNote.setText("添加记录");
                tv_addNote.setVisibility(View.VISIBLE);
                tv_queryNote.setText("查询记录");
                tv_queryNote.setVisibility(View.VISIBLE);


                tv_queryNote_subT.setVisibility(View.GONE);
                tv_queryNote_addT.setVisibility(View.GONE);
            } else {
                ll_settings.setVisibility(View.GONE);
            }

        } else if (v.getId() == R.id.tv_settings_delAllData) {      //删除所有记录
            tv_settings_delDataCustom.setVisibility(View.GONE);
            ll_settings_delAllData.setVisibility(View.VISIBLE);
            tv_settings_delAllData.setTextColor(Color.parseColor("#4662D9"));


        } else if (v.getId() == R.id.tv_settings_delAllData_sure) {     //确定——判断密码
            //密码正确
            if (et_password.getText().toString().trim().equals(passWord)){
                new AlertDialog.Builder(this)
                .setTitle("确定要删除所有记录吗？")
                    .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            helper.deleteAllData();
                            Toast.makeText(MainActivity.this, "数据删除成功", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .setNegativeButton("取消", null)
                    .show();


                et_password.setText("");
                tv_settings_delAllData.setTextColor(Color.rgb(0, 0, 0));
                ll_settings_delAllData.setVisibility(View.GONE);
                tv_settings_delDataCustom.setVisibility(View.VISIBLE);
                ll_settings.setVisibility(View.GONE);
            } else {        //密码错误

                et_password.setText("");
                tv_settings_delAllData.setText("密码错误!");
                tv_settings_delAllData.setTextColor(Color.rgb(255, 0, 0));

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        tv_settings_delAllData.setText("删除所有记录");
                        tv_settings_delAllData.setTextColor(Color.parseColor("#4662D9"));

                    }
                }, 1000);
            }

        } else if (v.getId() == R.id.tv_settings_delAllData_cancel) {       //取消——判断密码
            ll_settings_delAllData.setVisibility(View.GONE);
            tv_settings_delAllData.setTextColor(Color.rgb(0, 0, 0));
            tv_settings_delDataCustom.setVisibility(View.VISIBLE);
            ll_settings.setVisibility(View.GONE);
        } else if (v.getId() == R.id.tv_settings_delDataCustom) {        //自定义删除
            ll_settings.setVisibility(View.GONE);


        } else if (v.getId() == R.id.tv_addNote) {      //添加
            tv_queryNote.setVisibility(View.GONE);
            tv_queryNote.setText("查询记录");


            //导入最后一次的记录
            String[] lastRecord = helper.queryLastRecord();
            et_videoName.setText(lastRecord[0]);
            sp_videoCategory.setSelection(adapter.getPosition(lastRecord[1]));
            et_videoNumber.setText(lastRecord[2]);
            tv_addNote_lastRecord.setText(lastRecord[3]);


            ll_addNote_lastRecord.setVisibility(View.VISIBLE);

            ll_addNote.setVisibility(View.VISIBLE);
            ll_showData.setVisibility(View.GONE);
            ll_settings.setVisibility(View.GONE);


            tv_queryNote_subT.setVisibility(View.GONE);
            tv_queryNote_addT.setVisibility(View.GONE);
        } else if (v.getId() == R.id.tv_addNote_sure) {     //确定
            addDataToDB();
            ll_addNote_lastRecord.setVisibility(View.GONE);
        } else if (v.getId() == R.id.tv_addNote_cancel) {       //取消
            et_videoName.setText("");
            et_videoNumber.setText("");
            tv_addNote.setText("添加记录");
            tv_addNote.setTextColor(Color.rgb(0, 0, 0));

            ll_addNote.setVisibility(View.GONE);
            ll_showData.setVisibility(View.GONE);

            tv_queryNote.setVisibility(View.VISIBLE);
            ll_addNote_lastRecord.setVisibility(View.GONE);
        } else if (v.getId() == R.id.tv_addNote_clear) {        //清空
            et_videoName.setText("");
            et_videoNumber.setText("");
            sp_videoCategory.setSelection(0);
            ll_addNote_lastRecord.setVisibility(View.GONE);
        } else if (v.getId() == R.id.tv_queryNote) {        //查询
            if (tv_queryNote.getText().toString().trim().equals("查询记录")) {
                ll_showData.setVisibility(View.VISIBLE);

                this.numberOfItems = 5;
                showListView(this.numberOfItems);
                tv_queryNote.setText("关闭查询");

                tv_queryNote_subT.setText("记录-3");
                tv_queryNote_addT.setText("记录+3");
                tv_queryNote_subT.setVisibility(View.VISIBLE);
                tv_queryNote_addT.setVisibility(View.VISIBLE);

//                if (numberOfItems < 3){
//                    tv_queryNote_subT.setEnabled(false);
//                } else {
//                    tv_queryNote_subT.setEnabled(true);
//                }
            } else {
                tv_queryNote.setText("查询记录");
                ll_showData.setVisibility(View.GONE);
                tv_queryNote_subT.setVisibility(View.GONE);
                tv_queryNote_addT.setVisibility(View.GONE);
                this.numberOfItems = 0;
            }

            ll_settings.setVisibility(View.GONE);
        } else if (v.getId() == R.id.tv_queryNote_subT){        //减少三条记录
            if (this.numberOfItems > 3){
                this.numberOfItems -= 3;
                if (this.numberOfItems < 3) tv_queryNote_subT.setText("记录-1");
            } else if (this.numberOfItems <= 3){
                this.numberOfItems -= 1;
                if (this.numberOfItems == 0){
                    this.numberOfItems = 0;
                    tv_queryNote_subT.setVisibility(View.GONE);
                }
            }
            showListView(this.numberOfItems);
        } else if (v.getId() == R.id.tv_queryNote_addT){        //增加三条记录
            if (this.numberOfItems < (lastRecordById - 3)){
                this.numberOfItems += 3;
                showListView(this.numberOfItems);
            }

        }

    }
}