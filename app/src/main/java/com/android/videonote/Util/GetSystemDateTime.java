package com.android.videonote.Util;

import java.util.Date;
import java.text.SimpleDateFormat;

public class GetSystemDateTime {
    Date date;
    SimpleDateFormat sdf;

    /*
        获取系统日期
     */
    public String getSystemData(){
        date = new Date();
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }


    /*
        获取系统当前时间
     */
    public String getSystemTime(){
        date = new Date();
        sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(date);
    }


    /*
        获取系统当前日期和时间
    */
    public String getSystemDateTime() {
        date = new Date();
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }
}
