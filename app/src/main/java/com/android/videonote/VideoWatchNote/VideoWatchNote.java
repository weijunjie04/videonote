package com.android.videonote.VideoWatchNote;

public class VideoWatchNote {
    public int _id;
    public String videoName;
    public String videoCoCategory;
    public int videoNumber;
    public String dateTime;


    public VideoWatchNote() {

    }


    public VideoWatchNote(String videoName, String videoCoCategory, int videoNumber, String dateTime) {
        this.videoName = videoName;
        this.videoCoCategory = videoCoCategory;
        this.videoNumber = videoNumber;
        this.dateTime = dateTime;
    }


    public VideoWatchNote(int _id, String videoName, String videoCoCategory, int videoNumber, String dateTime) {
        this._id = _id;
        this.videoName = videoName;
        this.videoCoCategory = videoCoCategory;
        this.videoNumber = videoNumber;
        this.dateTime = dateTime;
    }



    @Override
    public String toString() {
        return "VideoWatchNote{" +
                "_id='" + _id + '\'' +
                ", videoName='" + videoName + '\'' +
                ", videoCoCategory='" + videoCoCategory + '\'' +
                ", videoNumber=" + videoNumber +
                ", dateTime='" + dateTime + '\'' +
                '}';
    }
}
