package com.android.videonote.VideoWatchNote;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.videonote.R;
import com.android.videonote.VideoWatchNote.VideoWatchNote;

import java.util.List;

// 自定义适配器类
public class VideoWatchNoteAdapter extends BaseAdapter {
    private List<VideoWatchNote> list;
    private Context context;

    TextView tv_videoName, tv_videoNumber, tv_videoDateTime;

    public VideoWatchNoteAdapter(Context context, List<VideoWatchNote> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
            tv_videoName = convertView.findViewById(R.id.tv_videoName);
            tv_videoNumber = convertView.findViewById(R.id.tv_videoNumber);
            tv_videoDateTime = convertView.findViewById(R.id.tv_videoDateTime);
        }


        VideoWatchNote videoWatchNote = list.get(position);
        if (tv_videoName != null) {
            tv_videoName.setText(videoWatchNote.videoName);
        }
        if (tv_videoNumber != null) {
            tv_videoNumber.setText(String.valueOf(videoWatchNote.videoNumber));
        }
        if (tv_videoDateTime != null) {
            tv_videoDateTime.setText(videoWatchNote.dateTime);
        }

        return convertView;
    }
}
